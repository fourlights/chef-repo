sains-test Cookbook
===================
Test chef cookbook for Sainsbury's

Requirements
------------

#### packages
- `Docker` - sains-test needs Docker to run the nodes.
- `Nginx` - Loadbalancer


Attributes
----------
TODO: List your cookbook attributes here.

e.g.
#### sains-test::default

Usage
-----
#### sains-test::default
Just include `sains-test` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[sains-test]"
  ]
}
```

Contributing
------------
License and Authors
-------------------
Authors: Dan Carter
