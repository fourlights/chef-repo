#
# Cookbook Name:: sains-test
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#
#
#docker_service 'default' do
#  action [:create, :start]
#end

#Pull latest nginx
docker_image 'nginx' do
  tag 'latest'
  action :pull
#  notifies :redeploy, 'docker_container[web_node]'
end

docker_image 'centos' do
  tag 'latest'
  action :pull
  #notifies :redeploy, 'docker_container[app_node_1]'
  #notifies :redeploy, 'docker_container[app_node_2]'
end

docker_image 'web_node' do
  tag 'latest'
  source '/root/chef-repo/cookbooks/sains-test/files/web_node/'
  action :build
end

docker_image 'app_node' do
  tag 'latest'
  source '/root/chef-repo/cookbooks/sains-test/files/app_node/'
  action :build
end

# Run container exposing ports
docker_container 'web_node_1' do
  repo 'web_node'
  port '80:80'
  host_name 'myapp1'
  domain_name 'test'
end


docker_container 'app_node_1' do
  repo 'app_node'
  port '9998:8484'
  host_name 'app_node_1'
  domain_name 'test'
  links ['web_node_1']
end

docker_container 'app_node_2' do
  repo 'app_node'
  port '9999:8484'
  host_name 'app_node_2'
  domain_name 'test'
  links ['web_node_1']
end

execute 'redeploy_links' do
  command 'touch /marker_container_redploy_links'
  creates '/marker_container_redploy_links'
  notifies :redeploy, 'docker_container[web_node_1]', :immediately
  notifies :redeploy, 'docker_container[app_node_1]', :immediately
  notifies :redeploy, 'docker_container[app_node_2]', :immediately
  action :run
end
