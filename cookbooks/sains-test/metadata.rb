name             'sains-test'
maintainer       'DAN CARTER'
maintainer_email 'DCARTER@EVOLVERE-TECH.CO.UK'
license          'All rights reserved'
description      'Installs/Configures sains-test'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'docker', '~> 2.0'
